#include "queue.h"
#include <iostream>

//This function will make a new queue with the data it receives
void initQueue(queue* q, unsigned int size)
{
	q->element = new int[size];
	q->size = size;
	q->place = 0;
	newQueue(q);
}

//This function will enter each cell in the array the value: -1
void newQueue(queue* q)
{
	for (int i = 0; i < q->size; i++)
	{
		q->element[i] = -1;
	}
}

//This function will delete from the heap the array
void cleanQueue(queue* q)
{
	delete[] q->element;
}

//O(1)
//This function gets queue, value (int) and will add the value to the array
void enqueue(queue* q, unsigned int newValue)
{
	if (q->size == q->place)
	{
		std::cout << "There is no place left in the queue to enter a value" << std::endl;
	}
	else
	{
		q->element[q->place] = newValue;
		q->place++;
	}
}

//O(n)
//This function gets queue and will return the value of the cell that delete from the array
int dequeue(queue* q)
{
	int val = q->element[0];
	int temp_val = 0;
	int i = 0;
	for (i = 0; i < q->place - 1; i++)
	{
		temp_val = q->element[i + 1];
		q->element[i] = temp_val;
	}
	q->element[i] = -1;
	q->place--;
	return val;
}

//Prints all the values in the array
void print(queue* q)
{
	for (int i = 0; i < q->size; i++)
	{
		std::cout << "q->element[" << i << "] = " << q->element[i] << std::endl;
	}
	std::cout << std::endl;
}
