#include "stack.h"
#include "utils.h"
#include "linkedList.h"
#include <iostream>
using namespace std;

//This function gets array (nums - int), size of the array and change the order of the array
void reverse(int* nums, unsigned int size)
{
	stack* s = new stack;
	initStack(s);
	for (int i = 0; i < size; i++)
	{
		push(s, nums[i]);
	}
	for (int i = 0; i < size; i++)
	{
		nums[i] = pop(s);
	}
}

//This function gets values from the user, create a new array on the heap and changes the order |(opposite to what the user entered)
int* reverse10()
{
	int* arr = new int[10];
	for (int i = 0; i < 10; i++)
	{
		cout << "Please enter an integer value: ";
		cin >> arr[i];
	}
	reverse(arr, 10);
	return arr;
}

