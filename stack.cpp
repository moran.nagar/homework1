#include "stack.h"
#include "linkedList.h"
#include <iostream>

//This function gets stack, value (int) and will create a new node with the new value
void push(stack* s, unsigned int element)
{
	if (s->first == NULL)
	{
		s->first = new linkedList;
		s->first->value = element;
		s->first->next = NULL;
	}
	else
	{
		s->first = add(s->first, element);
	}
}

//This function gets stack and will return the value of the node that delete from the linked list
int pop(stack* s)
{
	int val = -1;
	if (s->first != NULL)
	{
		val = s->first->value;
		s->first = s->first->next;
	}
	return val;
}

//This function will make a new linked list in the stack
void initStack(stack* s)
{
	s->first = NULL;
}

//This function will delete from the heap the linked list
void cleanStack(stack* s)
{
	linkedList** temp1 = &s->first;
	linkedList* temp2 = NULL;
	while ((*temp1)->next != NULL)
	{
		temp2 = (*temp1)->next;
		delete (*temp1);
		*temp1 = temp2;
	}
}

//Prints all the values in the linked list
void print(stack* s)
{
	linkedList* temp = s->first;
	while (temp != NULL)
	{
		std::cout << "q->linedlist = " << temp->value << std::endl;
		temp = temp->next;
	}
	
	std::cout << "q->linedlist = null" << std::endl;
	std::cout << std::endl;
}
