#include "linkedList.h"
#include <iostream>

//This function gets the first node in the list, value (int) and will create a new node with the new value and make the node to be the first on the list and return the new list
linkedList* add(linkedList* list, unsigned int newValue)
{
	linkedList* temp = new linkedList;
	temp->value = newValue;
	temp->next = list;
	return temp;
}
//This function gets the first node in the list, deletes it and returns the value of the first node (if the list is empty the value is -1)
int remove(linkedList* list)
{
	int val = -1;
	if (list != NULL)
	{
		val = list->value;
		list = list->next;
	}
	return val;
}

